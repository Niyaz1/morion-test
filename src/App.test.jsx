import React from 'react';
import {configure, render} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Datepicker from './Datepicker/Datepicker';

configure({adapter: new Adapter()});

describe('Datepicker', () => {

  const dataVerify = {
    dateFormat: 'dd.MM.yyyy',
    placeholder: 'Введите дату',
    startDate: new Date(),
    componentState: {
      disabled: true,
      invalid: true
    },
    label: 'Дата:',
    locale: 'ru'
  };

  const snapshot = (data) => {
    const wrapper = render(
      <Datepicker 
        dateFormat={data.dateFormat} 
        placeholder={data.placeholder}
        selected={data.startDate}
        componentStateDisabled={data.componentState.disabled}
        componentStateInvallid={data.componentState.invalid}
        label={data.label}
        locale={data.locale}
      />
    );
    it(`dateFormat = ${data.dateFormat}, 
        placeholder = ${data.placeholder}, 
        selected = ${data.startDate}, 
        componentStateDisabled = ${data.componentState.disabled},
        componentStateInvallid = ${data.componentState.invalid},
        label = ${data.label},
        locale = ${data.locale}
        `, () => {
          expect(wrapper).toMatchSnapshot();
    });
  };

  snapshot(dataVerify);

});