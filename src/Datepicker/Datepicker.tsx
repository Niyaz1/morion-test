import React, {forwardRef, FunctionComponent, ReactNode} from 'react';
import DatePicker, {registerLocale} from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './Datepicker.sass';
import ru from 'date-fns/locale/ru';
import getDay from "date-fns/getDay";
import format from "date-fns/format";
import {log} from "util";

registerLocale('ru', ru);

interface IProps {
    selected?: Date,
    componentStateDisabled?: boolean,
    componentStateInvalid?: boolean,
    locale?: string,
    label?: string,
    placeholder?: string,
    dateFormat?: string,
    onChangeDate?: any,
    onClick?: () => any,
    value?: any,
    onClickBottomDate?: any
}

const cls = ['datepicker'];
type RefButton = HTMLButtonElement;
type RefDiv = HTMLDivElement;

const isWeekday = (date: Date) => {
    const day = getDay(date);
    return day !== 0;
};

const InputStyled = forwardRef<RefButton, IProps>((props, ref) => (
    <button ref={ref} className="datepicker__input" onClick={props.onClick}>
        {props.value}
    </button>
));

const BottomDatepicker = forwardRef<RefDiv, IProps>((props, ref) => {
    return (
        <div ref={ref} className="datepicker__bottom-date" onClick={props.onClick}>
            {format(props.value, 'dd.MM.yyyy')}
        </div>
    )
});

const Datepicker: FunctionComponent<IProps> = (props) => {
    if (props.componentStateDisabled) {
        cls.push('datepicker--disabled')
    }

    if (props.componentStateInvalid) {
        cls.push('datepicker--invalid')
    }

    if (props.locale === 'en') {
        cls.push('datepicker--en')
    }

    return (
        <div className={cls.join(' ')}>
            <label className='datepicker__label'>
                <span className='datepicker__label-text'>{props.label}</span>
                <DatePicker
                    selected={props.selected}
                    onChange={props.onChangeDate}
                    placeholderText={props.placeholder}
                    dateFormat={props.dateFormat}
                    disabled={props.componentStateDisabled}
                    customInput={<InputStyled/>}
                    locale={props.locale}
                    filterDate={isWeekday}
                >
                    <BottomDatepicker value={props.selected} onClick={props.onClickBottomDate}/>
                </DatePicker>
            </label>
        </div>
    )
}

export default Datepicker