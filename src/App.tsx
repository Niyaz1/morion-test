import React, {Component} from 'react';
import './App.sass';
import Datepicker from './Datepicker/Datepicker';

interface Iprops {
}

interface DatepickerState {
    dateFormat: string,
    placeholder: string,
    startDate: Date,
    disabled: boolean,
    invalid: boolean,
    label: string,
    locale: string
}


class App extends Component<Iprops, DatepickerState> {

    constructor(props: Iprops) {
        super(props);

        this.state = {
            dateFormat: 'dd.MM.yyyy',
            placeholder: 'Введите дату',
            startDate: new Date(),
            disabled: false,
            invalid: false,
            label: 'Дата:',
            locale: 'en'
        }

    }

    public handleChange = (date: Date) => {
        this.setState({
            startDate: date
          }
        );
    };

    render() {

        console.log();

        return (
            <div className='App'>
                <h1>React Datepicker</h1>

                <Datepicker
                    dateFormat={this.state.dateFormat}
                    placeholder={this.state.placeholder}
                    selected={this.state.startDate}
                    componentStateDisabled={this.state.disabled}
                    componentStateInvalid={this.state.invalid}
                    label={this.state.label}
                    locale={this.state.locale}
                    onChangeDate={(date: Date) => this.handleChange(date)}
                />
            </div>
        )
    }

}

export default App;
